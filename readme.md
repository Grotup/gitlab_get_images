# Get Images Gitlab

## Installation

Créer un fichier .env à la racine du projet qui contient :

```properties
GITLAB_TOKEN=YOUR_TOKEN
GITLAB_BASE_URL=BASE_URL_GITLAB
```

Installer les dépendances :

```shell
npm install
```

## Exécution

Lancer la commande :

```shell
node index.js
```

Trois questions sont posées :

`Nom du projet` pour faire une recherche dans gitlab sur les projets
Ensuite la liste de résultat est proposée pour choisir le projet

`Nom du repo` pour choisir parmi les repos dans lesquels aller chercher les images

Ensuite, la liste des liens d'image est affichée.
