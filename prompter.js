const inquirer = require('inquirer');

const demanderTypeImage = () => {
    return inquirer
        .prompt([
            {
                type: 'list',
                name: 'typeImage',
                message: 'Snapshot ? production ?',
                choices: ['snapshot', 'production']
            }
        ]);
}

const demanderProjet = () => {
    return inquirer
        .prompt([
            {
                type: 'input',
                name: 'projet',
                message: 'Nom du projet',
            },
        ]);
};

const proposerProjets = (listeProjets) => {
    return inquirer
        .prompt([
            {
                type: 'list',
                name: 'projet',
                message: 'Nom du projet',
                choices: listeProjets.map(projet => {
                    return {
                        name: projet.name,
                        value: projet.id,
                    };
                })
            },
        ]);
}

const demanderChoixRepository = (listeRepos) => {
    return inquirer
        .prompt([
            {
                type: 'list',
                name: 'repo',
                message: 'Nom du Repo',
                choices: listeRepos.map(repo => {
                    return {
                        name: repo.name,
                        value: repo.id,
                    };
                })
            },
        ]);
}

exports.demanderProjet = demanderProjet;
exports.proposerProjets = proposerProjets;
exports.demanderTypeImage = demanderTypeImage;
exports.demanderChoixRepository = demanderChoixRepository;
