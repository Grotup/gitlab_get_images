const gitlab = require('./gitlab-api');
const prompter = require('./prompter');

var state = {
};

prompter.demanderProjet()
    .then((answers) => {
        return gitlab.chercherProjet(answers.projet);
    })
    .then((listeProjetsTrouves) => {
        return prompter.proposerProjets(listeProjetsTrouves);
    })
    .then((answers) => {
        state.projet = answers.projet;
        return gitlab.recupererRespositoriesProjet(state.projet)
    })
    .then((listeRepos) => {
        return prompter.demanderChoixRepository(listeRepos);
    })
    .then((answers) => {
        state.repo = answers.repo;
        return gitlab.listerImagesRepository(state.repo, state.projet)
    })
    .then((reponse) => {
        console.table(reponse.map(
            (image) => {
                return {
                    name: image.name,
                    location: image.location,
                };
            }
        ))
    })
    .catch((error) => {
        console.error('Something went wrong.', error.message, error.stack);
    });
