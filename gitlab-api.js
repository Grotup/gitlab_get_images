const axios = require('axios');
const { load } = require('dotenv');
require('dotenv').config()

axios.defaults.headers.common['Authorization'] = `Bearer ${process.env.GITLAB_TOKEN}`;
axios.defaults.baseURL = process.env.GITLAB_BASE_URL;

const recupererRespositoriesProjet = (nomProjet, typeImage) => {
    return chercherRespositoriesProjet(nomProjet)
        .then((reponse) => {
            return reponse.data;
        })
}

const listerImagesRepository = (idRepository, idProjet) => {
    return axios.get(`/api/v4/projects/${idProjet}/registry/repositories/${idRepository}/tags`).then((reponse) => {
        return reponse.data
    });
}

const chercherRespositoriesProjet = (nomProjet) => {
    return axios.get(`/api/v4/projects/${nomProjet}/registry/repositories`);
};

const listerGroupes = () => {
    return axios.get('/api/v4/groups').then(reponse => reponse.data);
}

const chercherProjet = (nomProjetAChercher) => {
    return axios.get(`/api/v4/projects?search=${nomProjetAChercher}`).then(reponse => reponse.data);
}

exports.recupererRespositoriesProjet = recupererRespositoriesProjet;
exports.listerImagesRepository = listerImagesRepository
exports.listerGroupes = listerGroupes;
exports.chercherProjet = chercherProjet;
